#include<iostream>
#include<queue>
using namespace std;

struct node {
	int data;
	struct node* left;
	struct node* right;
};

class BinaryTree {
public :
  //Giving memory to Node and setting up data value and pointers
  node* initializeNode(int d) {
  	 node* n = (node*)malloc(sizeof(node));
  	 n->data=d;
  	 n->left=NULL;
  	 n->right=NULL;
  	 return n;
  }

//+AB  -> root,left,right
  void preorderTraversal(node* root) {
     if(root == NULL) return;
     cout<<root->data<<" ";
     preorderTraversal(root->left);
     preorderTraversal(root->right);
  }

  //AB+  -> left,right, root
  void postorderTraversal(node* root) {

  }

  //A+B  -> left,root,right
  void inorderTraversal(node * root) {

  }

  //Breadth First Traversal
  void levelOrderTraversal(node* root) {
  	   if(!root) return;
       queue<node*> q;      //Queue for storing Node Pointers
       q.push(root);        //Queue contains it's first entry which is root

       while(!q.empty()) {
       	 node* temp = q.front();
       	 cout<<temp->data<<" ";        //Printing Values
       	 q.pop();

       	 if(temp->left)
       	 	 q.push(temp->left);

       	 if(temp->right)
       	    q.push(temp->right); 	
       }
  }
};


int main() {
    BinaryTree bt;
 
    node* root;     
    //bt.preorderTraversal(root);    Will give segementation fault , since no memory was allocated

    /*
    Created a Binary Tree 
         1
       /   \
      2     3
     /     /
    4     5
    
    */

    root = bt.initializeNode(1);
    root->left = bt.initializeNode(2);
    root->right = bt.initializeNode(3);
    root->left->left = bt.initializeNode(4);
    root->right->left = bt.initializeNode(5);

    //Calling preorder
    bt.preorderTraversal(root);
    cout<<endl<<endl;

    //Calling Level order
    bt.levelOrderTraversal(root);

    return 0;
}

